Rails.application.routes.draw do
  
  controller :sessions do
    get 'login' => :new, :as => :login
    post 'login' => :create, :as => :authenticate
    get 'auth/shopify/callback' => :callback
    get 'logout' => :destroy, :as => :logout
  end

  controller :proxy do
    get 'proxy' => :index
    post 'proxy' => :index
  end

  post 'refer_a_friend_form_style', controller: 'refer_a_friend_form_styles', action: :create_or_update, :defaults => { :format => 'json' }

  root :to => 'home#index'

end
