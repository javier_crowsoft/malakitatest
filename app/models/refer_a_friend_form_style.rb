class ReferAFriendFormStyle < ActiveRecord::Base

  def self.default_style
    new(
        :input_width => "100%",
        :text_color => "#333333",
        :background_color => "#ffffff",
        :button_color => "#ffffff",
        :border_color => "#cccccc",
        :border_width => "1px",
        :font_family => "'HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
        :font_size => "1.15em",
        :font_width => "normal",
    )
  end

  def email_label_style
    label_style
  end

  def email_style
    input_style
  end

  def message_label_style
    label_style
  end

  def message_style
    input_style
  end

  def submit_style
    return "" unless id
    [
      "color: " + text_color,
      "background-color: " + button_color,
      "font-family: " + font_family,
      "font-size: " + font_size,
      "font-weight: " + font_width
    ].join("; ")
  end
  
  def input_style
    return "" unless id
    [
        "width: " + input_width,
        "color: " + text_color,
        "background-color: " + background_color,
        "border-color: " + border_color,
        "border-width: " + border_width,
        "font-family: " + font_family,
        "font-size: " + font_size,
        "font-weight: " + font_width
    ].join("; ")
  end

  def label_style
    return "" unless id
    [
        "color: " + text_color,
        "font-family: " + font_family,
        "font-size: " + font_size,
        "font-weight: " + font_width
    ].join("; ")
  end

end
