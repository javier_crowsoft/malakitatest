class ReferAFriendEmail < ActiveRecord::Base

  def self.get_customer_profiles
    query = %{
      select customer_name, emails.customer_email, emails_sent, friend_email, product_title, product_absolute_url, to_char(created_at,'DD Mon YYYY') created_at
      from refer_a_friend_emails emails
      inner join (select customer_email, count(*) emails_sent from refer_a_friend_emails group by customer_email) totals
              on emails.customer_email = totals.customer_email
      order by emails_sent desc, customer_email
      ;
    }

    self.connection().select_all query
  end

end