class RequestValidation

  def self.verify_request_source(query_string)

    query_hash = Rack::Utils.parse_query(query_string)

    # Remove and save the "signature" entry
    signature = query_hash.delete("signature")

    sorted_params = sort_params(query_hash)

    signature == calculate_signature(sorted_params)
  end

  def self.create_signature(query_string)

    query_hash = Rack::Utils.parse_query(query_string)

    sorted_params = sort_params(query_hash)

    calculate_signature(sorted_params)
  end

  private

  def self.sort_params(params)
    params.collect{ |k, v| "#{k}=#{Array(v).join(',')}" }.sort.join
  end

  def self.calculate_signature(sorted_params)
    OpenSSL::HMAC.hexdigest(OpenSSL::Digest::Digest.new('sha256'), ShopifyApp.configuration.secret, sorted_params)
  end
end