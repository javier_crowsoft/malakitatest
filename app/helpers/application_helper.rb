module ApplicationHelper

  def monetize_cents(amount)
    amount = BigDecimal(amount) / 100
    if amount == amount.floor
      number_to_currency amount, precision: 0
    else
      number_to_currency amount
    end
  end

end
