class ReferAFriendMailer < ApplicationMailer
  add_template_helper(ApplicationHelper)

  default from: 'javier@crowsoft.com.ar'

  def refer_a_product(email)
    puts ActionMailer::Base.smtp_settings
    @email = email
    subject = "#{@email.customer_name} recommends you the product #{@email.product_title}"
    @friend_name = @email.friend_email.partition('@').first
    mail(to: @email.friend_email, subject: subject)
  end
end
