class HomeController < AuthenticatedController
  def index
    @shop = ShopifyAPI::Shop.current
    shop = Shop.find_by_shopify_domain(@shop.myshopify_domain)
    @refer_a_friend_form_style = ReferAFriendFormStyle.find_by_shop_id(shop.id)
    @refer_a_friend_form_style ||= ReferAFriendFormStyle.default_style
    @emails = ReferAFriendEmail.order("created_at desc").limit(10)
    @emails_by_products = ReferAFriendEmail.select("count(*) as count, product_title, product_absolute_url").group("product_title, product_absolute_url").order("count").limit(10)
    @customer_profiles = ReferAFriendEmail.get_customer_profiles
  end
end
