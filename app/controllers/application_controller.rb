class ApplicationController < ActionController::Base
  include ShopifyApp::Controller
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token, if: :skip_token_verification

  def render_json_response(type, hash = nil)
    unless [ :ok, :redirect, :error ].include?(type)
      raise "Invalid json response type: #{type}"
    end

    hash ||= {}

    # To keep the structure consistent, we'll build the json
    # structure with the default properties.
    #
    # This will also help other developers understand what
    # is returned by the server by looking at this method.
    default_json_structure = {
        :status => type,
        :html => nil,
        :message => nil,
        :to => nil }.merge(hash)

    render_options = {:json => default_json_structure}
    render_options[:status] = 400 if type == :error

    render(render_options)
  end

  protected

  def skip_token_verification
    json_request? or proxy_request?
  end

  def json_request?
    request.format.json?
  end

  def proxy_request?
    request.path == "/proxy"
  end

end
