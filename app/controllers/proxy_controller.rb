class ProxyController < ApplicationController
  before_filter :set_headers
  before_filter :verify_request_source
  before_filter :restrict_access
  before_action :set_default_response_format

  def index
    if params[:p] == "send_email"
      send_email
    elsif params[:p] == "refer_a_friend_snippet"
      refer_a_friend_snippet
    else
      render :layout => false, :content_type => 'application/liquid'
    end
  end

  def send_email

    customer = params[:customer]
    product = params[:product]
    message = params[:message]
    shopify_domain = params[:myshopify_domain]
    shop = Shop.find_by_shopify_domain(shopify_domain)
    shop_id = shop.id if shop

    email = ReferAFriendEmail.new(
        :customer_name => customer[:name],
        :customer_email => customer[:email],
        :product_title => product[:title],
        :product_description => product[:description],
        :product_price => product[:price],
        :product_absolute_url => product[:absolute_url],
        :shopify_domain => shopify_domain,
        :shop_id => shop_id,
        :friend_email => message[:email],
        :message => message[:description]
    )

    email.save!

    ReferAFriendMailer.refer_a_product(email).deliver_later

    respond_to do |format|
      format.json { render_json_response :ok }
    end
  end

  def refer_a_friend_snippet

    snippet = %{
{% if customer and product and shop and customer.orders_count > 0 %}
<form action="{{ form_action }}" class="refer_a_friend_form" id="refer_a_friend_form" method="post">

  <div class="note form-success" id="refer-a-friend-succes-message" style="display: none;">
    Your recommendation has been successfully sent!
  </div>
  <div class="note form-error" id="refer-a-friend-error-message" style="display: none;">
    <p>An error has occurred in the server.</p>
    <p>Please try again.</p>
  </div>
  <div class="note form-error" id="refer-a-friend-invalid-email-message" style="display: none;">
    <p>Please insert a valid email!</p>
  </div>

  <label for="email" style="{{ email_label_style }}">Your friend's email address</label>
  <input id="email" name="message[email]" type="text" style="{{ email_style }}"/><br />
  <label for="description" style="{{ message_label_style }}">Description</label>
  <textarea id="description" name="message[description]" type="text" style="{{ message_style }}">
  </textarea><br />
  <input name="customer[name]" value="{{ customer.first_name }} {{ customer.last_name }}" type="hidden">
  <input name="customer[email]" value="{{ customer.email }}" type="hidden">
  <input name="product[title]" value="{{ product.title }}" type="hidden">
  <input name="product[description]" value="{{ product.description | escape }}" type="hidden">
  <input name="product[price]" value="{{ product.price }}" type="hidden">
  <input name="product[absolute_url]" value="{{ shop.url }}{{ product.url }}" type="hidden">
  <input name="myshopify_domain" value="{{ myshopify_domain }}" type="hidden">
  <input name="commit" type="submit" value="Send email" style="{{ submit_style }}"/>
</form>
{% endif %}

<script type="text/javascript">

  $('#refer_a_friend_form').submit(function(e) {

    $('#refer-a-friend-error-message').css('display', 'none');

    if(checkEmail($("#email").val())) {
      $('#refer-a-friend-invalid-email-message').css('display', 'none');
      var valuesToSubmit = $(this).serialize();
      $.ajax({
        type: "POST",
        url: getActionUrl($(this).attr('action')),
        data: valuesToSubmit,
        dataType: "JSON",
        success:function(data) {
          $('#refer-a-friend-succes-message').css('display', 'block');
          $("#email").val("");
          $("#description").val("");
        }
        ,error: function (xhr, desc, err) {
          $('#refer-a-friend-error-message').css('display', 'block');
        }
      });
      return false; // prevents normal behaviour
    } else {
      e.preventDefault();
      $('#refer-a-friend-invalid-email-message').css('display', 'block');
      return false;
    }
  });

  function getActionUrl(path) {
  	var url = window.location.href
    var arr = url.split("/");
    return arr[0] + "//" + path;
  }

  function checkEmail(emailAddress) {
    var pattern = /^(([^<>()[\\]\\.,;:\\s@\\"]+(\\.[^<>()[\\]\\.,;:\\s@\\"]+)*)|(\\".+\\"))@(([^<>()[\\]\\.,;:\\s@\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\"]{2,})$/i;
    return pattern.test(emailAddress);
  };

</script>
    }

    shopify_domain = params[:myshopify_domain]
    shop = Shop.find_by_shopify_domain(shopify_domain)
    shop_id = shop.id if shop

    refer_a_friend_form_style = ReferAFriendFormStyle.find_by_shop_id(shop_id)
    refer_a_friend_form_style ||= ReferAFriendFormStyle.new

    # TODO: get proxy subpath from app's configuration page or ShopifyAPI call ( if they add one :P )
    #
    url = "#{shopify_domain}/apps/proxy?p=send_email"

    snippet.gsub!("{{ form_action }}", url)
    snippet.gsub!("{{ myshopify_domain }}", params[:myshopify_domain])
    snippet.gsub!("{{ email_label_style }}", refer_a_friend_form_style.email_label_style)
    snippet.gsub!("{{ email_style }}", refer_a_friend_form_style.email_style)
    snippet.gsub!("{{ message_label_style }}", refer_a_friend_form_style.message_label_style)
    snippet.gsub!("{{ message_style }}", refer_a_friend_form_style.message_style)
    snippet.gsub!("{{ submit_style }}", refer_a_friend_form_style.submit_style)

    send_data snippet, :filename => 'refer-a-friend.liquid'
  end

  private

  def verify_request_source
    render :file => "public/401.html", :status => :unauthorized unless RequestValidation.verify_request_source(request.query_string)
  end

  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS, HEAD'
    headers['Access-Control-Allow-Headers'] = '*,x-requested-with,Content-Type,If-Modified-Since,If-None-Match'
    headers['Access-Control-Max-Age'] = '86400'
  end

  def restrict_access
    if params[:p] == "refer_a_friend_snippet"
      shopify_domain = params[:myshopify_domain]
      shop = Shop.find_by_shopify_domain(shopify_domain)
      @shop_id = shop.id if shop
      render :file => "public/401.html", :status => :unauthorized unless @shop_id
    end
  end

  def set_default_response_format
    request.format = :json if params[:p] == "send_email"
  end

end