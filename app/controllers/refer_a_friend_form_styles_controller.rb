class ReferAFriendFormStylesController < ApplicationController

  before_filter :verify_request_source
  before_filter :restrict_access

  def create_or_update
    refer_a_friend_form_style = ReferAFriendFormStyle.find_by_shop_id(@shop_id)
    refer_a_friend_form_style ||= ReferAFriendFormStyle.new(:shop_id => @shop_id)

    refer_a_friend_form_style.input_width = params[:refer_a_friend_style_width]
    refer_a_friend_form_style.text_color = params[:refer_a_friend_style_text_color]
    refer_a_friend_form_style.background_color = params[:refer_a_friend_style_background_color]
    refer_a_friend_form_style.button_color = params[:refer_a_friend_style_button_color]
    refer_a_friend_form_style.border_color = params[:refer_a_friend_style_border_color]
    refer_a_friend_form_style.border_width = params[:refer_a_friend_style_border_width]
    refer_a_friend_form_style.font_family = params[:refer_a_friend_style_font_family]
    refer_a_friend_form_style.font_size = params[:refer_a_friend_style_font_size]
    refer_a_friend_form_style.font_width = params[:refer_a_friend_style_font_width]

    refer_a_friend_form_style.save!

    respond_to do |format|
      format.json { render_json_response :ok }
    end
  end

  private

  def verify_request_source
    query_string = "shop=#{params[:myshopify_domain]}&timestamp=#{params[:timestamp]}&signature=#{params[:signature]}"
    render :file => "public/401.html", :status => :unauthorized unless RequestValidation.verify_request_source(query_string)
  end

  def restrict_access
    shopify_domain = params[:myshopify_domain]
    shop = Shop.find_by_shopify_domain(shopify_domain)
    @shop_id = shop.id if shop
    head :unauthorized unless @shop_id
  end
end