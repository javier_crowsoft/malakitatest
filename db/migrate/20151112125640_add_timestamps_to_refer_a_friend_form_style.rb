class AddTimestampsToReferAFriendFormStyle < ActiveRecord::Migration
  def change
    add_column(:refer_a_friend_form_styles, :created_at, :datetime)
    add_column(:refer_a_friend_form_styles, :updated_at, :datetime)
  end
end
