class AddShopIdToReferAFriendEmail < ActiveRecord::Migration
  def change
    add_reference :refer_a_friend_emails, :shop, index: true
  end
end
