class AddShopifyDomainToReferAFriendEmail < ActiveRecord::Migration
  def change
    add_column :refer_a_friend_emails, :shopify_domain, :string
  end
end
