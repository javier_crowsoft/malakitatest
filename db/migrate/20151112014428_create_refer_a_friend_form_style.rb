class CreateReferAFriendFormStyle < ActiveRecord::Migration
  def change
    create_table :refer_a_friend_form_styles do |t|
      t.string :input_width
      t.string :text_color
      t.string :background_color
      t.string :button_color
      t.string :border_color
      t.string :border_width
      t.string :font_family
      t.string :font_size
      t.string :font_width
      t.integer :shop_id, :null => false, :references => [:shops, :id]
    end
  end
end
