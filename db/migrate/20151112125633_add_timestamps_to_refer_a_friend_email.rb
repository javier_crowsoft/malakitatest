class AddTimestampsToReferAFriendEmail < ActiveRecord::Migration
  def change
    add_column(:refer_a_friend_emails, :created_at, :datetime)
    add_column(:refer_a_friend_emails, :updated_at, :datetime)
  end
end
