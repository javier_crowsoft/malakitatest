class CreateReferAFriendEmail < ActiveRecord::Migration
  def change
    create_table :refer_a_friend_emails do |t|
      t.string :customer_name
      t.string :customer_email
      t.string :product_title
      t.string :product_description
      t.integer :product_price
      t.string :product_absolute_url
      t.string :friend_email
      t.string :message
    end
  end
end
