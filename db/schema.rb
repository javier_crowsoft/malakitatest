# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151112125640) do

  create_table "refer_a_friend_emails", force: :cascade do |t|
    t.string   "customer_name"
    t.string   "customer_email"
    t.string   "product_title"
    t.string   "product_description"
    t.integer  "product_price"
    t.string   "product_absolute_url"
    t.string   "friend_email"
    t.string   "message"
    t.string   "shopify_domain"
    t.integer  "shop_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "refer_a_friend_emails", ["shop_id"], name: "index_refer_a_friend_emails_on_shop_id", using: :btree

  create_table "refer_a_friend_form_styles", force: :cascade do |t|
    t.string   "input_width"
    t.string   "text_color"
    t.string   "background_color"
    t.string   "button_color"
    t.string   "border_color"
    t.string   "border_width"
    t.string   "font_family"
    t.string   "font_size"
    t.string   "font_width"
    t.integer  "shop_id",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shops", force: :cascade do |t|
    t.string   "shopify_domain", null: false
    t.string   "shopify_token",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shops", ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true, using: :btree

end
